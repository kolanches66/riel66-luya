
# Sitis Luya default project


### Installation

use `composer install`

copy files:
```
docker/env.php -> configs/env.php
docker/env-local-db.php -> configs/env-local-db.php
```

setup database `configs/env-local-db.php`

```
./vendor/bin/luya migrate
./vendor/bin/luya import
./vendor/bin/luya admin/setup
```

## Docker

## Изменяем настройки базы данных в файлах docker-compose.yml и в папке docker - env-local-db.php 


Копируем или переименовываем конфиги 
```
cp .env.dist .env
cp docker/env* configs/
```

Создаем контейнеры

```
docker-compose build
docker-compose up -d
docker-compose ps
```

Установка проекта

```
docker-compose run luya_composer install
docker-compose exec luya_php luya  migrate --migrationPath="@yii/rbac/migrations"
docker-compose exec luya_php luya  migrate
docker-compose exec luya_php luya  import
docker-compose exec luya_php luya  admin/setup
```

Перейти по ссылке

```
http://localhost:8080
http://localhost:8080/admin
```

Для доступа к контейнеру
```
docker ps
# Пример 
CONTAINER ID        IMAGE                                                 COMMAND                  CREATED             STATUS              PORTS                              NAMES
793bdc1982ac        luyadefault_luya_php                                  "docker-php-entrypoi…"   18 minutes ago      Up 17 minutes       9000/tcp                           luyadefault_luya_php_1

docker exec -it <your_docker_container_id>  bash
```

## Styleguide
Ссылка для styleguide
```
/styleguide
```
Пароль
```
555
```