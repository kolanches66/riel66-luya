<?php
/**
 * Created by PhpStorm.
 * User: vlasov
 * Date: 27.07.2017
 * Time: 22:03
 */


require 'vendor/luyadev/luya-deployer/luya.php';

//set('shared_files', [
//    'public_html/robots.txt',
//    'public_html/.htaccess',
//]);

task('deploy:webpack', function () {
    upload(
        __DIR__ . "/vendor/sitis/config/assets/WebpackAsset.php",
        '{{deploy_path}}/current/vendor/sitis/config/assets//WebpackAsset.php'
    );
});
//after('cleanup:deployfile', 'deploy:webpack');

server('prod', 'h13.netangels.ru', 22)
    ->user('c14982')
    ->identityFile()
    ->forwardAgent()
    ->stage('prod')
    ->env('deploy_path', '/home/c14982/riel66.ru/source'); // Define the base path to deploy your project to.

set('repository', ' git@bitbucket.org:kolanches66/riel66-luya.git');