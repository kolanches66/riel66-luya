<?php
/**
 * View file for block: IconsBlock
 *
 * File has been created with `block/create` command.
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<?php if (!empty($this->varValue('icons'))): ?>
    <div class="bankcards">
        <?php foreach ($this->varValue('icons') as $icon): ?>
            <div class="bankcard">
                <?php if (isset($icon['imageId']) && $image =\Yii::$app->storage->getImage($icon['imageId'])): ?>
                    <div class="bankcard__icon">
                        <img src="<?= $image->sourceAbsolute ?>">
                    </div>
                <?php endif; ?>
                <?php if (isset($icon['caption'])): ?>
                    <h4 class="bankcard__title"><?= $icon['caption'] ?></h4>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>