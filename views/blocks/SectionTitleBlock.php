<?php
/**
 * View file for block: SectionTitleBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<h2 class="sect__header"><?= $this->varValue('title') ?></h2>