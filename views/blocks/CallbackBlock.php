<?php
/**
 * View file for block: CallbackBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="m-centred">
    <div class="cont cont_50p">
        <div class="sect__headercontainer">
            <h2 class="sect__header">Заинтересованы?</h2>
            <p class="sect__description">Дам бесплатную консультацию по телефону</p>
        </div>
        <div class="callback">
            <form class="callback__form" action="https://riel66.ru/wp-content/themes/themeriel66/handlers/callback.php" method="post" autocomplete="off">
                <div class="callback__fields">
                    <div class="callback__fields__line">
                        <input class="callback__textbox" type="text" name="callback_name" placeholder="Имя">
                    </div>
                    <div class="callback__fields__line">
                        <input class="callback__textbox" type="text" name="callback_phone" placeholder="Телефон" maxlength="16">
                    </div>
                    <div class="callback__success">Спасибо! Заявка отправлена. Ожидайте звонка</div>
                    <div class="callback__fields__line">
                        <div class="callback__policy">
                            <a target="_blank" href="http://localhost:8080/?page_id=3">Нажимая «Отправить», вы соглашаетесь предоставить Ваши персональные данные на обработку.</a>
                        </div>
                    </div>
                </div>
                <div class="callback__buttons">
                    <button class="callback__buttonsubmit js-callback-submit" name="submit" type="submit">Отправить</button>
                </div>
            </form></div>
    </div>
</div>