<?php
/**
 * View file for block: ReviewsBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<?php if (!empty($this->varValue('reviews'))): ?>
<!--    <div class="owl-carousel owl-theme reviews__carousel glide">-->
    <div class="glide">
        <div data-glide-el="track" class="glide__track">
            <ul class="glide__slides reviews__track">
                <?php foreach ($this->varValue('reviews') as $review): ?>
                    <li class="glide__slide">
                        <div class="card__container_review">
                            <div class="card__text card card_review">
                                <div class="card__author"><?= $review['author'] ?></div>
                                <div class="card__body"><?= \yii\helpers\Markdown::process($review['content']) ?></div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div data-glide-el="controls">
            <button class="slider__arrow slider__arrow--prev glide__arrow glide__arrow--prev" data-glide-dir="<">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                    <path d="M0 12l10.975 11 2.848-2.828-6.176-6.176H24v-3.992H7.646l6.176-6.176L10.975 1 0 12z"></path>
                </svg>
            </button>

            <button class="slider__arrow slider__arrow--next glide__arrow glide__arrow--next" data-glide-dir=">">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24">
                    <path d="M13.025 1l-2.847 2.828 6.176 6.176h-16.354v3.992h16.354l-6.176 6.176 2.847 2.828 10.975-11z"></path>
                </svg>
            </button>
        </div>
        <div class="slider__bullets glide__bullets" data-glide-el="controls[nav]">
    </div>
<?php endif; ?>