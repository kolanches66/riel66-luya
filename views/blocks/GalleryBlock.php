<?php
/**
 * View file for block: GalleryBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<?php if (!empty($this->varValue('images'))): ?>
    <div class="certs">
        <?php foreach ($this->varValue('images') as $icon): ?>
            <div class="cert">
                <?php if (isset($icon['imageId']) && $image =\Yii::$app->storage->getImage($icon['imageId'])): ?>
                    <a data-fancybox="gallery" href="<?= $image->sourceAbsolute ?>">
                        <img class="cert__image" src="<?= $image->sourceAbsolute ?>">
                    </a>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>