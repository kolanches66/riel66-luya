<?php
/**
 * View file for block: SectionBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<section class="sect">
    <?= $this->placeholderValue('header'); ?>
    <div class="sect__content">
        <?= $this->placeholderValue('content'); ?>
    </div>
</section>