<?php
/**
 * View file for block: ServicesBlock 
 *
 * File has been created with `block/create` command. 
 *
 *
 * @var \luya\cms\base\PhpBlockView $this
 */
?>

<div class="services__container_for_list">
    <ul class="services__list">
    <?php foreach ($this->varValue('services') as $item): ?>
        <li class="services__listitem">
            <div class="card card_question">
                <div class="card__icon">
                    <?php $icon_source = \Yii::$app->storage->getFile($item['icon'])->sourceAbsolute ?>
                    <img class="card__icon__image" src="<?= $icon_source ?>">
                </div>
                <div class="card__content">
                    <div class="card__header"><?= $item['name'] ?></div>
                    <div class="card__description"><?= isset($item['description']) ? $item['description'] : '' ?></div>
                    <div class="card__body"></div>
                </div>
            </div>
        </li>
    <?php endforeach; ?>

    </ul>
</div>
