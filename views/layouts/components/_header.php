<div class="sitewrapper">

    <section>
        <div class="whoami">
            <div class="whoami__description">Агент по недвижимости</div>
            <div class="whoami__name">
                <a class="whoami__name__link">Пономарева<br>Марина</a>
            </div>
        </div>
    </section>

    <header class="welcome" style="background-image: url('/images/header-bg.jpg');">
        <div class="cont cont_welcome">
            <div class="welcome__container">
                <div class="welcome__avatar">
                    <img class="welcome__avatar__image" src="/images/header-photo.png" alt="">
                </div>
                <div class="welcome__slogans">
                    <div class="welcome__slogan">
                        <h1 class="welcome__slogan__experience">1000</h1>
                        <h2 class="welcome__slogan__work">довольных клиентов</h2>
                    </div>
                    <div class="welcome__slogan">
                        <h1 class="welcome__slogan__experience">12</h1>
                        <h2 class="welcome__slogan__work">лет в недвижимости</h2>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="menu-panel">
        <div class="menu-panel-container">
            <div class="menu-panel__burger">
                <span></span><span></span><span></span>
            </div>
            <nav class="menu-panel__nav">
                <ul class="menu-panel__nav-list">
                    <li class="menu-panel__nav-list-item active">
                        <a class="active"  href="tel:+79028723894">+7 902 8723 894</a>
                    </li>
                    <?php foreach (Yii::$app->menu->findAll(['depth' => 1, 'container' => 'default']) as $item): /* @var $item \luya\cms\menu\Item */ ?>
                        <li class="menu-panel__nav-list-item" <?= \Yii::$app->response->statusCode == 200 && $item->isActive ? 'active' : '' ?>">
                            <a href="<?= $item->link; ?>"><?= $item->title; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        </div>
    </div>
