</div>

<footer class="footer">
    <div class="">
        <div class="footer__copyright">© Пономарева Марина, агент по недвижимости, 2019</div>
        <div class="footer__contacts">
            <div class="footer__contact">
                <span class="footer__icon"><i class="fas fa-phone"></i></span>
                <span><a class="m-link" href="tel:+79028723894">+7 902 87 23 894</a></span>
            </div>
            <div class="footer__contact">
                <span class="footer__icon"><i class="fas fa-envelope"></i></span>
                <span><a class="m-link" href="mailto:ponomareva@riel66.ru">ponomareva@riel66.ru</a></span>
            </div>
        </div>
    </div>
</footer>