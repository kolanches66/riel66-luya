<?php
/**
 * @var string $content
 * @var \luya\web\View $this
 */

use luya\cms\models\NavItem;

?>

<?php $this->beginContent('@app/views/layouts/_clear.php'); ?>


<?= $this->render('components/_header'); ?>
<?= $this->render('components/_breadcrumbs'); ?>
<?= $content ?>
<?= $this->render('components/_footer'); ?>



<?php $this->endContent(); ?>
