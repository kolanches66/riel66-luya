<?php

$config = [
    'name' => 'mponomareva',
    'id' => 'mponomareva',
    'siteTitle' => 'Агент по недвижимости Марина Пономарева',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'cms',
    'sourceLanguage' => 'ru-RU',
    'language' => 'ru-RU',
    'timezone' => 'Asia/Yekaterinburg',
    'bootstrap' => [
        'config',
        'cEmail',
        'cConfig',
    ],
    'modules' => [
        'blocks' => [
            'class' => 'app\modules\blocks\frontend\Module',
        ],
        'blocksadmin' => [
            'class' => 'app\modules\blocks\admin\Module',
        ],
        'contact' => [
            'class' => app\modules\contact\Module::class,
        ],
    ],
//    'components' => [
//        'cConfig' => [
//            'items' => [
//                'footerText' => [
//                    'label' => 'Текст в футере',
//                    'value' => '',
//                    'inputOptions' => [
//                        'type' => \luya\admin\base\TypesInterface::TYPE_TEXTAREA,
//                    ],
//                ],
//            ]
//        ],
//    ]
];

return \yii\helpers\ArrayHelper::merge(require('_config/base.php'), $config);