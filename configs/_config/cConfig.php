<?php

return [
    'emailForLeads' => [
        'label' => 'Почта для заявок',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
            ['email'],
        ],
    ],
    'emailForLeadsCopy' => [
        'label' => 'Почта для заявок копия',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['email'],
        ],
    ],
    'ccEmailForLeads' => [
        'label' => 'Почта для заявок скрытая копия',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
            ['email'],
        ],
    ],
    'yandexAccount' => [
        'path' => 'modules.uniform.yandexAccount',
        'label' => '№ Яндекс счетчика',
        'rules' => [
            ['required'],
            ['integer', 'min' => 10000000],
        ],
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_NUMBER,
        ],
    ],
    'phone' => [
        'label' => 'Телефон',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'address' => [
        'label' => 'Адрес',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'siteName' => [
        'label' => 'Название сайта',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
        ],
    ],
    'siteDesc' => [
        'label' => 'Описание сайта',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
        ],
    ],
    'suffixTitle' => [
        'label' => 'Суффикс заголовка',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
        ],
    ],
    'suffixDivider' => [
        'label' => 'Разделитель до суффикса',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
        ],
    ],
    'vkontakte' => [
        'label' => 'Вконтакте',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['string'],
        ],
    ],
    'instagram' => [
        'label' => 'Instagram',
        'value' => '',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['string'],
        ],
    ],
];