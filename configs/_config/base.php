<?php

return [
    'modules' => [
        'admin' => [
            'class' => 'luya\admin\Module',
            'secureLogin' => false,
            'interfaceLanguage' => 'ru',
            'userIdleTimeout' => 604800,
            'jsonCruft' => false
        ],
        'cms' => [
            'class' => 'luya\cms\frontend\Module',
            'contentCompression' => false,
        ],
        'cmsadmin' => [
            'class' => 'luya\cms\admin\Module',
        ],
        'config' => [
            'class' => '\sitis\config\admin\Module',
        ],
        'plugins' => [
            'class' => '\sitis\plugins\admin\Module',
        ],
    ],
    'components' => [
        'storage' => [
            'class' => 'sitis\config\components\LocalFileSystem',
        ],
        'mail' => [
            'isSMTP' => true,
            'host' => 'mail.sitisit.ru',  // mail.sitisit.ru
            'smtpSecure' => 'tls',              // ssl
            'port' => 587,               // tls: 587 (or 25), ssl: 465
            'username' => 'dev@sitisit.ru',
            'password' => 'z74gpgRhvvhAtiY',
            'from' => 'dev@sitisit.ru',
            'fromName' => 'Development',
        ],
        'composition' => [
            'hidden' => true,
            'default' => ['langShortCode' => 'ru'], // the default language for the composition should match your default language shortCode in the langauge table.
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache', // use: yii\caching\FileCache
        ],
        'assetManager' => [
            'class' => 'luya\web\AssetManager',
            'linkAssets' => true,
            'appendTimestamp' => true,
            'hashCallback' => function ($path) {
                return hash('md4', $path);
            },
        ],
        'formatter' => [
            'numberFormatterSymbols' => [
                NumberFormatter::CURRENCY_SYMBOL => '&#8381;',
            ],
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 0,
                NumberFormatter::MAX_FRACTION_DIGITS => 2,
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'itemTable' => '{{%auth_items}}',
            'itemChildTable' => '{{%auth_item_children}}',
            'assignmentTable' => '{{%auth_assignments}}',
            'ruleTable' => '{{%auth_rules}}',
        ],
        'cConfig' => [
            'class' => 'yii2tech\config\Manager',
            'autoRestoreValues' => true,
            'cacheId' => 'config-manager-cConfig',
            'storage' => [
                'class' => 'sitis\config\components\StorageDb',
                'filter' => ['section' => 'config'],
                'types' => [
                    'socials' => \sitis\config\components\StorageDb::TYPE_JSON,
                    'yandexAccount' => \sitis\config\components\StorageDb::TYPE_INTEGER,
                ],
            ],
            'items' => require('cConfig.php')
        ],
        'cEmail' => [
            'class' => 'yii2tech\config\Manager',
            'autoRestoreValues' => true,
            'cacheId' => 'config-manager-cEmail',
            'storage' => [
                'class' => 'sitis\config\components\StorageDb',
                'filter' => ['section' => 'email'],
            ],
            'items' => require('cEmail.php')
        ],
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
];