<?php

use luya\admin\helpers\Angular;

return [
    'isSMTP' => [
        'path' => 'components.mail.isSMTP',
        'label' => 'Использовать SMTP сервер',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_SELECT,
            'options' => luya\admin\helpers\Angular::optionsArrayInput([
                1 => 'Да',
                0 => 'Нет',
            ]),
        ],
    ],
    'host' => [
        'path' => 'components.mail.host',
        'label' => 'Хост',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'smtpSecure' => [
        'path' => 'components.mail.smtpSecure',
        'label' => 'Smtp Secure (tls или ssl)',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'port' => [
        'path' => 'components.mail.port',
        'label' => 'Порт (tls: 587 (или 25), ssl: 465)',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'username' => [
        'path' => 'components.mail.username',
        'label' => 'Логин',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'password' => [
        'path' => 'components.mail.password',
        'label' => 'Пароль',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_PASSWORD,
        ],
    ],
    'from' => [
        'path' => 'components.mail.from',
        'label' => 'От (почта)',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
    ],
    'fromName' => [
        'path' => 'components.mail.fromName',
        'label' => 'От (имя)',
        'inputOptions' => [
            'type' => \luya\admin\base\TypesInterface::TYPE_TEXT,
        ],
        'rules' => [
            ['required'],
        ],
    ],
];