<?php

//defined('YII_ENV') or define('YII_ENV', 'local');
defined('YII_ENV') or define('YII_ENV', 'prod');
defined('YII_DEBUG') or define('YII_DEBUG', true);

$config = [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;port=33306;dbname=riel66',
            'username' => 'root',
            'password' => 'maxim123',
            'charset' => 'utf8',
        ],
    ]
];

return \yii\helpers\ArrayHelper::merge($config, require('_config.php'));