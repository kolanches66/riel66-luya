<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Section Block.
 *
 * File has been created with `block/create` command. 
 */
class SectionBlock extends PhpBlock
{
    public $isContainer = true;
    public $cacheEnabled = true;
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Секция';
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'crop_16_9'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'bgcolor', 'label' => 'Цвет фона', 'type' => self::TYPE_COLOR],
                ['var' => 'fullWidth', 'label' => 'На всю ширину', 'type' => self::TYPE_CHECKBOX]
            ],
            'placeholders' => [
                [
                    ['var' => 'header', 'cols' => 12, 'label' => 'Заголовок'],
                    ['var' => 'content', 'cols' => 12, 'label' => 'Содержимое'],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     *
     */
    public function admin()
    {

    }
}