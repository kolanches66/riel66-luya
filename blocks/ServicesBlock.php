<?php

namespace app\blocks;

use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;
use luya\cms\helpers\BlockHelper;

/**
 * Services Block.
 *
 * File has been created with `block/create` command. 
 */
class ServicesBlock extends PhpBlock
{
    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;
    
    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Services Block';
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'extension'; // see the list of icons on: https://design.google.com/icons/
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'services', 'label' => 'Услуги', 'type' => self::TYPE_MULTIPLE_INPUTS, 'options' => [
                    ['var' => 'icon', 'label' => 'Иконка', 'type' => self::TYPE_FILEUPLOAD],
                    ['var' => 'name', 'label' => 'Название', 'type' => self::TYPE_TEXT],
                    ['var' => 'description', 'label' => 'Примечание', 'type' => self::TYPE_TEXTAREA],
                    ['var' => 'content', 'label' => 'Содержание', 'type' => self::TYPE_TEXTAREA],
                    ['var' => 'link', 'label' => 'Ссылка', 'type' => self::TYPE_TEXT],
                ]]
            ]
        ];
    }
    
    /**
     * {@inheritDoc} 
     *
    */
    public function admin()
    {
        return '<h5 class="mb-3">Services Block</h5>' .
            '<table class="table table-bordered">' .
            '</table>';
    }
}