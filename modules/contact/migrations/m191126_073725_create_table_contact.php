<?php

use yii\db\Migration;

/**
 * Class m191126_073725_create_table_contact
 */
class m191126_073725_create_table_contact extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ext_contact}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'phone' => $this->string(14),
            'email' => $this->string(150),
            'content' => $this->text(),
            'created_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ext_contact}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191126_073725_create_table_contact cannot be reverted.\n";

        return false;
    }
    */
}
