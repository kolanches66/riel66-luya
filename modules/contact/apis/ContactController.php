<?php

namespace app\modules\contact\apis;

use app\modules\contact\models\CallbackForm;
use app\modules\contact\models\Contact;
use app\modules\contact\models\PriceCalculatorForm;
use app\modules\contact\models\PrintCalculatorForm;

/**
 * Contact Controller.
 *
 * File has been created with `crud/create` command.
 */
class ContactController extends \luya\admin\ngrest\base\Api
{

    public $authOptional = ['callback'];

    public $modelClass = 'app\modules\contact\models\Contact';

    public function actionCallback()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $form = new CallbackForm();
        if ($form->load(\Yii::$app->request->post(), '')) {
            return $form->send();
        }
        return ['errors' => $form->getErrors()];
    }

}
