<?php

namespace app\modules\contact\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Contact.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $phone
 * @property text $content
 * @property integer $created_at
 */
class Contact extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ext_contact}}';
    }

    public function either($attribute_name, $params)
    {
        $field1 = $this->getAttributeLabel($attribute_name);
        $field2 = $this->getAttributeLabel($params['other']);
        if (empty($this->$attribute_name) || empty($this->{$params['other']})) {
            $this->addError($attribute_name, Yii::t('user', "either {$field1} or {$field2} is required."));
        }
    }

    public function sendEmail($content)
    {
        $config = \Yii::$app->cConfig->getItemValues();
        if (!isset($config['emailForLeads']) || !$config['emailForLeads']) {
            return ['error' => 'Ошибка на сервере. Не удалось отправить заявку.'];
        }

        $mail = \Yii::$app->mail;
        $mail->layout = '@app/views/mail/layout.php';
        $html = \Yii::$app->view->render('@app/views/mail/test.php', ['content' => $content]);

        $mail->compose("Заявка с сайта", $html);

        $mail->address($config['emailForLeads']);
        if (isset($config['ccEmailForLeads']) && $config['ccEmailForLeads']) {
            $mail->bccAddresses([$config['ccEmailForLeads']]);
        }
        $mail->addReplyTo(conf('replyTo', $mail->from), conf('emailFromName', $mail->fromName));
        $mail->fromName = conf('emailFromName', $mail->fromName);
        $mail->from = conf('emailFrom', $mail->from);

        if (!$mail->send()) {
            return ['error' => 'Ошибка на сервере. Не удалось отправить заявку. '];
        }

        return ["message" => "Спасибо! Заявка отправлена. Мы скоро свяжемся с вами."];
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'api-contact';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Имя'),
            'phone' => Yii::t('app', 'Телефон'),
            'created_at' => Yii::t('app', 'Дата заявки'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'name' => 'text',
            'phone' => 'text',
            'created_at' => 'datetime',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['name', 'phone', 'created_at']],
            [['create', 'update'], ['name', 'phone', 'created_at']],
            ['delete', false],
        ];
    }
}
