<?php


namespace app\modules\contact\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class CallbackForm extends Model
{
    public $name;
    public $phone;

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Имя'),
            'phone' => \Yii::t('app', 'Телефон'),
        ];
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['phone'], 'string', 'length' => 11]
        ];
    }

    public function send() {
        $model = new Contact();

        $name = $this->name;
        $content = "Имя: $name<br>Телефон: $this->phone";

        $model->phone = $this->phone;
        $model->content = $content;
        $model->created_at = time();

        if ($this->validate() && $model->validate() && $model->save()) {
            return $model->sendEmail($content);
        }
        return ['errors' => ArrayHelper::merge($this->getErrors(), $model->getErrors())];
    }

}
