<?php

namespace app\modules\contact;

class Module extends \luya\admin\base\Module
{
    public $apis = [
        'api-contact' => 'app\modules\contact\apis\ContactController',
    ];

    public function getMenu()
    {
        return (new \luya\admin\components\AdminMenuBuilder($this))
            ->node('Заявки', 'extension')
            ->group('Формы')
                ->itemApi('Заказать звонок', 'contact/contact/index', 'label', 'api-contact');
    }

}
