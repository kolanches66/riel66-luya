<?php

namespace app\modules\contact\controllers;

/**
 * Contact Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class ContactController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\contact\models\Contact';
}