<?php
/**
 * View file for block: CkEditorBlock
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 *
 * @param $this->varValue('text');
 * @param $this->cfgValue('container_class');
 *
 * @param $this->cfgValue('default_css');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>

<div class="<?= $this->cfgValue('container_class', 'block-text') ?>">
    <?= $this->varValue('text') ?>
</div>
