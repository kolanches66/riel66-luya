<?php

namespace app\modules\blocks\frontend\blocks;


use app\modules\blocks\components\base\BaseBlock;
use luya\cms\base\PhpBlock;
use luya\cms\frontend\blockgroups\ProjectGroup;

/**
 * Ck Editor Block.
 *
 * File has been created with `block/create` command on LUYA version 1.0.0-RC4.
 */
class CkEditorBlock extends PhpBlock
{
    /**
     * @inheritDoc
     */
    public function name()
    {
        return 'Редактор';
    }

    public function blockGroup()
    {
        return ProjectGroup::class;
    }

    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'border_color'; // see the list of icons on: https://design.google.com/icons/
    }

    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'vars' => [
                ['var' => 'text', 'label' => 'Текст', 'type' => 'zaa-ckeditor'],
            ],
            'cfgs' => [
                ['var' => 'container_class', 'label' => 'Основной CSS класс', 'type' => self::TYPE_TEXT],
            ]
        ];
    }

    /**
     * {@inheritDoc}
     *
     * @param {{vars.text}}
     */
    public function admin()
    {
        return '<h5 class="mb-3">Редактор</h5>' .
            '<table class="table table-bordered">' .
            '{% if vars.text is not empty %}' .
            '<tr><td colspan="2">{{vars.text}}</td></tr>' .
            '{% endif %}'.
            '</table>';
    }
}