plugins
=======
Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sitis/plugins "*"
```

or add

```
"sitis/plugins": "*"
```

to the require section of your `composer.json` file.