<?php

namespace app\modules\blocks\admin\plugins;

use luya\admin\ngrest\base\Plugin;

class EditorPlugin extends Plugin
{
    /**
     * @var string Html5 placholder attribute value to set and example for the user
     */
    public $placeholder;

    /**
     * @inheritdoc
     */
    public function renderList($id, $ngModel)
    {
        return $this->createListTag($ngModel);
    }

    /**
     * @inheritdoc
     */
    public function renderCreate($id, $ngModel)
    {
        return $this->createFormTag('zaa-froala-editor', $id, $ngModel, ['placeholder' => $this->placeholder, 'name' => $this->name]);
    }

    /**
     * @inheritdoc
     */
    public function renderUpdate($id, $ngModel)
    {
        return $this->renderCreate($id, $ngModel);
    }


}