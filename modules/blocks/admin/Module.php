<?php

namespace app\modules\blocks\admin;

/**
 * Plugins Admin Module.
 *
 */
class Module extends \luya\admin\base\Module
{
    public function getAdminAssets()
    {
        return [
            'app\modules\blocks\admin\assets\AdminAsset',
        ];
    }
}