/* zephir angular admin */
/* resolve controller: https://github.com/angular-ui/ui-router/wiki#resolve */

(function () {
    "use strict";

    zaa.directive("zaaCkeditor", function() {
        return {
            restrict: "E",
            scope: {
                "model": "=",
                "options": "=",
                "label": "@label",
                "i18n": "@i18n",
                "id": "@fieldid",
                "placeholder": "@placeholder",
                "name": "@name"
            },
            controller: ['$scope', '$timeout', '$http', function($scope, $timeout, $http) {

                CKEDITOR.config.allowedContent = true;
                CKEDITOR.config.extraPlugins = 'font';

                $timeout(function () {
                    var ck = CKEDITOR.replace($('#' + $scope.id).get(0));

                    ck.on('change', function () {
                        $scope.$apply(function () {
                            $scope.model = ck.getData();
                        });
                    });

                    ck.on('mode', function() {
                        if (this.mode == 'source') {
                            var editable = ck.editable();
                            editable.attachListener(editable, 'input', function() {
                                $scope.$apply(function () {
                                    $scope.model = ck.getData();
                                });
                            });
                        }
                    });

                    $scope.$render = function (value) {
                        ck.setData($scope.model);
                    };

                    $scope.$on("$destroy",function() {
                        ck.destroy();
                    });
                });
            }],

            template: function() {
                return '<div class="form-group form-side-by-side" ng-class="{\'input--hide-label\': i18n}"><div ng-if="name" class="form-side form-side-label"><label for="{{id}}">{{label}}</label></div><div class="form-side"><textarea id="{{id}}" insert-paste-listener ng-model="model" type="text" class="form-control" auto-grow></textarea></div></div>';
            }
        }
    });

})();