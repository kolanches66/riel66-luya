<?php

namespace app\modules\blocks\admin\assets;

class AdminAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/modules/blocks/admin/resources';

    public $js = [
        'ckeditor.js',
    ];

    public $publishOptions = [
        'forceCopy' => !YII_ENV_PROD,
    ];

    public $depends = [
        'luya\admin\assets\Main',
        'app\modules\blocks\admin\assets\CkeditorAsset',
    ];
}