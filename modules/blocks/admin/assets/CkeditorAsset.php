<?php

namespace app\modules\blocks\admin\assets;

class CkeditorAsset extends \luya\web\Asset
{
    public $sourcePath = '@bower/ckeditor';

    public $js = [
        'ckeditor.js',
    ];
}
