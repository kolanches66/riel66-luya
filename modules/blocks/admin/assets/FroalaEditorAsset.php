<?php

namespace app\modules\blocks\admin\assets;

class FroalaEditorAsset extends \luya\web\Asset
{
    public $sourcePath = '@bower/froala-editor';

    public $js = [
        'js/froala_editor.min.js',
    ];
}
