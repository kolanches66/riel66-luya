const Webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const devServerHost = 'http://localhost';
const devServerPort = 9000;

module.exports = merge(common, {
  mode: 'development',
  devtool: 'cheap-eval-source-map',

  output: {
    filename: '[name].js',
    chunkFilename: 'vendor.js',
    path: path.join(__dirname, '../dist'),
    publicPath: `${devServerHost}:${devServerPort}/dist`
  },

  devServer: {
    compress: true,
    port: devServerPort,
    overlay: {
      warnings: true,
      errors: true
    },
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
  plugins: [
    new Webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: 'vendors.css',
      publicPath: `${devServerHost}:${devServerPort}/dist`
    })
  ],
});
