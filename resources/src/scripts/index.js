import '../styles/index.scss';
import 'normalize.css';
import '@glidejs/glide/dist/css/glide.core.min.css';
import '@glidejs/glide/dist/css/glide.theme.min.css';
import Glide from '@glidejs/glide/dist/glide.min';
import IMask from 'imask';

window.jQuery = require('jquery');
require('@fancyapps/fancybox/dist/jquery.fancybox.min');
require('@fancyapps/fancybox/dist/jquery.fancybox.min.css');

if (document.querySelector('.glide') !== null) {
  new Glide('.glide', {
    slidesToShow: 1,
    draggable: true,
    arrows: {
      prev: '.glider-prev',
      next: '.glider-next'
    }
  }).mount();
}

const burger = document.querySelector('.menu-panel__burger');
const menuPanel = document.querySelector('.menu-panel');

burger.addEventListener('click', () => {
  menuPanel.classList.toggle('menu-panel--open');
});

// window.onscroll = () => {
//   const sticky = menuPanel.offsetTop;
//   if (window.pageYOffset >= sticky) {
//     menuPanel.classList.add('menu-panel--sticky');
//   }
// };

(function() {

  // const themeUrl = '/wp-content/themes/themeriel66';

  const errorBubble = 'callback__error-field';
  const errorTextbox = 'callback__textbox_error';
  const correctTextbox = 'callback__textbox_correct';

  const callbackName = 'callback_name';
  const callbackPhone = 'callback_phone';
  const callbackSubmit = 'js-callback-submit';

  const name = document.querySelector(`input[name="${callbackName}"]`);
  const phone = document.querySelector(`input[name="${callbackPhone}"]`);
  const fields = [name, phone];

  const phoneMask = IMask(phone, { mask: '+{7} 000 000 00 00' });

  const showError = (item, errorText) => {
    let errorField = item.parentNode.querySelector(`.${errorBubble}`);
    if (!errorField) {
      errorField = document.createElement('div');
      errorField.classList.add(errorBubble);
    }
    errorField.innerText = errorText;
    item.parentNode.appendChild(errorField);
    item.classList.remove(correctTextbox);
    item.classList.add(errorTextbox);
  };

  const removeError = (item) => {
    const errorField = item.parentNode.querySelector(`.${errorBubble}`);
    if (errorField !== null) {
      errorField.remove();
    }
    item.classList.remove(errorTextbox);
  };

  const validate = (item) => {
    const error = ((item) => {
      if (item.getAttribute('name') === callbackName) {
        if (item.value.length === 0) {
          return 'Введите имя';
        }
      }
      if (item.getAttribute('name') === callbackPhone) {
        if (phoneMask.unmaskedValue.length === 0) {
          return 'Введите номер телефона';
        }
        if (!phoneMask.masked.isComplete) {
          return 'Введите верный номер телефона';
        }
      }
      return false;
    })(item);

    if (error) {
      showError(item, error);
      return false;
    }

    removeError(item);
    item.classList.add(correctTextbox);
    return true;
  };

  async function submit(e) {
    e.preventDefault();

    let formData = new FormData();
    formData.append('name', name.value);
    formData.append('phone', phoneMask.unmaskedValue);

    const url = '/admin/api-contact/callback';
    const res = await fetch(url, { method: 'post', body: formData });
    const body = await res.json();

    console.log(body);

    if (body.message) {
      const success = document.querySelector('.callback__success');
      success.classList.toggle('callback__success--visible');
    }

  }

  if (name && phone) {
    fields.forEach((item) => {
      if (item.getAttribute('name') === callbackPhone) {
        item.addEventListener('focus', () => {
          item.setAttribute('placeholder', '+7 XXX XXX XX XX');
        });
        item.addEventListener('blur', () => {
          item.setAttribute('placeholder', 'Телефон');
        });
      }
      item.addEventListener('focus', removeError.bind(null, item));
      item.addEventListener('blur', validate.bind(null, item));
    });

    document.querySelector(`.${callbackSubmit}`).addEventListener('click', (e) => {
      e.preventDefault();
      let isValid = true;
      fields.forEach(field => {
        if (!validate(field)) {
          isValid = false;
        }
      });
      if (isValid) submit(e);
    });
  }

})();
